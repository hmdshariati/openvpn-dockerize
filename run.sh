#!/bin/bash
sudo docker create --name=openvpn-as \
--restart=always \
-v /home/docker/openvpn-as/config:/config \
-e INTERFACE=ens3 \
-e PGID=1004 -e PUID=1000 \
-e TZ=Europe/Warsaw \
--net=host --privileged \
linuxserver/openvpn-as
